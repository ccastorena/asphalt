#pragma once
#include <vector>
#include <memory>
#include "newton.h"
using std::shared_ptr;
using std::vector;
using std::shared_ptr;

using std::vector;

class AsphaltConstants
{
     public:
        AsphaltConstants() = delete;
        AsphaltConstants( const double Eaf, const double Af,
            const double R, const double M, const double logG0, const double Eac,
            const double Ac, const int nReplicates) :
        m_Eaf(Eaf), m_Af(Af), m_R(R), m_M(M), m_logG0(logG0), m_Eac(Eac),
        m_Ac(Ac), m_nReplicates(nReplicates){}
        const double m_Eaf;
        const double m_Af;
        const double m_R;
        const double m_M;
        const double m_logG0;
        const double m_Eac;
        const double m_Ac;
        const int m_nReplicates;
};

double gStar(const double time, const double temp,
    const AsphaltConstants& p);
double gStarDerivitive(const double time, const double temp,
    const AsphaltConstants& p);

void processSingleIteration( const shared_ptr<AsphaltConstants>& params,
	const vector<double>& temperatures, vector<double>& times, vector<double>& values);

void run(const shared_ptr<AsphaltConstants>& params, const vector<double>& temperatures, const char* outfile);
