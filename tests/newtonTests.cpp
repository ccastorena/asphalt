#include "catch.hpp"
#include "newton.h"

#define my_tolerance 1e-15

TEST_CASE("step", "[newton]")
{
    const auto f = [](const double x){return 100 - x;};
    const auto df = [](const double x){return -1;};
	const auto x = 76.1;
    REQUIRE(step(f, df, x) == Approx(x - f(x)/df(x))); 
}

TEST_CASE("find_root", "[newton]")
{
    const auto f = [](const double x){return 100 - x;};
    const auto df = [](const double x){return -1;};
    REQUIRE(findRootNewton(f, df, 50, my_tolerance) == Approx(100)); 
}

TEST_CASE("invert", "[newton]")
{
    const auto f = [](const double x){return  x*x;};
    const auto df = [](const double x){return 2*x;};
    const auto y = 27.0;
    const auto testVal = invertNewton(f, df, y, 5500, my_tolerance);
    REQUIRE(f(testVal) == Approx(y)); 
}

