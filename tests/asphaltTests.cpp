#include "catch.hpp"
#include "fileIO.h"
#include "newton.h"
#include "pavement.h"
#include <random>

std::random_device rd;
std::mt19937 gen(rd());
std::uniform_real_distribution<> dis(0, 365*20);
std::uniform_real_distribution<> disTemp(220, 350);

#define my_tolerance 1e-15

const auto hour = 1.0/24.0;
const double upper = 365*20;
const double lower = 0.0;
TEST_CASE("test function and derivitive", "[asphalt]" )
{
    const auto params = getParams("configs/config.json");
    const auto time = 300;
    const auto temp = 333.15;
    const auto functionVal = gStar(time, temp, *params);
    const auto derivitiveVal = gStarDerivitive(time, temp, *params);
    REQUIRE(2.7059512733 == Approx(functionVal));
    REQUIRE(0.0034579264 == Approx(derivitiveVal));
}

TEST_CASE("test inverse on simulated data", "[asphalt]" )
{
	const auto params = getParams("configs/config.json");
    for(int i = 0; i < 1; i++)
    {
        const auto temp = disTemp(gen);
        const auto time = dis(gen);
        const auto f = [=](const double t){return gStar(t, temp, *params);};
        const auto y = f(time);
        const auto inverse = invertBisect(f, y, lower, upper, my_tolerance);
        REQUIRE(time == Approx(inverse));
    }
}

TEST_CASE("test real data", "[asphalt]" )
{
    const auto params = getParams("configs/config.json");
    const auto data = getData("testData/data.json");
    auto time = 0.0;
    for(const auto temp : data)
    {
        const auto f = [=](const double t){return gStar(t, temp, *params);};
        const auto y = f(time);
        const auto inverse = invertBisect(f, y, lower, upper, my_tolerance);
        REQUIRE(time == Approx(inverse));
        time = inverse + hour;
    }
}
